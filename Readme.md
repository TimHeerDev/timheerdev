# run selenium grid
docker run -d -e SCREEN_WIDTH=1366 -e SCREEN_HEIGHT=768 -e SCREEN_DEPTH=24 -p 4444:4444 selenium/standalone-chrome

# run tests in docker 
docker pull heerdev/selenium:6 

docker run -it  --network="host" heerdev/selenium:6  /bin/bash

git clone -b selenium https://nupeten:2019develop@gitlab.com/nupeten/opensource.git

cd opensource/

mvn clean verify serenity:aggregate -Dwebdriver.remote.url=http://127.0.0.1:4444/wd/hub -Dwebdriver.remote.driver=chrome  -Dwebdriver.driver=chrome -Pserenity
